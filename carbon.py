import socket, time, ssl, re, os
import random
from time import strftime
from datetime import timedelta

startTime = time.time()
server = "192.168.200.152"
port = 25175
isSsl = True
channel = ["#vrach"]
nick = "Carbon"
owner = "graphene"
nl = "\n"
identifier = "!"
rules = dict()
cmdlist = [("help <page>", r"help( \d+)?", "Show this text.", lambda info: print_help(info["ch"], info["msg"])),
           ("about", r"about", "Show version information.", lambda info: send_prv_msg(info["ch"], about_txt)),
           ("dice", r"dice", "Show an random number between 1 and 6.",
            lambda info: send_prv_msg(info["ch"], str(random.randrange(1, 6)) + "!")),
           ("ping", r"ping", "Test the connection between the user and the bot.",
            lambda info: send_prv_msg(info["ch"], "Pong!")),
           ("time", r"time", "Show time in KST.", lambda info: send_prv_msg(info["ch"], strftime("%T"))),
           ("date", r"date", "Show today's date.", lambda info: send_prv_msg(info["ch"], strftime("%b %e %Y %A"))),
           ("uptime", r"uptime", "Show how long the bot is operating.",
            lambda info: send_prv_msg(info["ch"], str(timedelta(seconds=time.time() - startTime)))),
           ("echo <message>", r"echo .+", "Echo message.", lambda info: echo(info["ch"], info["msg"])),
           ("if <message> !echo <message>", r"if .+? !echo .+", "Say the message if specified message is sent.",
            lambda info: set_echo_rule(info["ch"], info["msg"])),
           ("removeif <message>", r"removeif .+", "Delete echoRule of the message.", lambda info: delete_echo_rule (info["ch"], info["msg"])),
           ("echoRules", r"echoRules", "Show current echo rules.",
            lambda info: [[send_prv_msg(info["ch"], '"' + key + '" -> ' + str(rules[key]))
                           for key in rules.keys()] if rules else send_prv_msg(info["ch"], "No echo rules.")]),
            ]

private_commands = [("join #\s+", lambda info: join("#" + info["msg"].split("#")[2].split(" ")[0])),
                    ("quit", lambda info: finalize()),
                    ("echoRaw .+?", lambda info: send(info["msg"].split("!echoRaw ")[1] + nl))]

more_help = "Type 'help <page>' to see more."
about_txt = "Carbon 0.0.9 Alpha" + nl + "IRC Bot by Graphene."
codec = "utf-8"
absolutePath = "/sdcard/"
logfile = open(absolutePath + "carbon.log", "a")


def print_help(ch, message):
    line = 4
    try:
        index = int(message.split("!help ")[1]) - 1
    except ValueError:
        send_prv_msg(ch, "Invalid argument: expecting number.")
        return
    except IndexError:
        index = 0

    start = index * line
    end = start + line
    
    maximum = int((len(cmdlist) - len(cmdlist) % line) / line) + 1
    if len(cmdlist) < end:
        end = len(cmdlist)
        if end <= start:
            send_prv_msg(ch, "Invalid number: use number below " + str(maximum)) + "."
            return

    send_prv_msg(ch, "Usage: !<command>")
    send_prv_msg(ch, "Commands: " + str(index + 1) + " out of " + str(maximum))
    for i in range(start, end):
        command, regex, description, func = cmdlist[i]
        send_prv_msg(ch, command + ": " + description)


def finalize():
    send("QUIT :Going down for maintenance, see you soon! - Carbon, IRC bot by Graphene.")
    logfile.close()
    quit()


def ping(msg):
    pong = msg.strip("PING :")
    send("PONG :" + pong + nl)


def join(ch):
    send("JOIN " + ch + nl)


def echo(ch, msg):
    send_prv_msg(ch, msg.split("!echo ")[1])


def set_echo_rule(name, message):
    identifier = message.split("!if ")[1].split(" !echo")[0]
    message = message.split("!echo ")[1]
    if identifier in rules:
        send_prv_msg(name, "Rule Already Exists.")
    else:
        rules[identifier] = message
        send_prv_msg(name, "Rule Saved!")

def delete_echo_rule(name, message):
    identifier = message.split("!removeif ")[1]
    if identifier not in rules:
        send_prv_msg(name, "Rule Does Not Exist.")
    else:
        del rules[identifier]
        send_prv_msg(name, "Rule Deleted!")

def send(content):
    sock.send(content.encode(codec))
    log(content)


def send_prv_msg(to, message):
    if not message.strip("\r\n").strip(" "):
        return
    for line in message.split(nl):
        message = line.strip(" ")
        send("PRIVMSG" + to + " :" + message + nl)


def find(message, target):
    return re.search(r":" + target, message)


def log(string=""):
    string = string.strip("\r\n").strip(" ")
    if not string:
        return
    logfile.write(strftime("%T %b %e %Y: ") + string + nl)
    logfile.flush()


def execute(name, message):
    for command in cmdlist:
        cmd, regex, desc, func = command
        if find(message, identifier + regex + "$"):
            info = {"ch": name, "msg": message}
            log(message)
            func(info)
            break

    for command in private_commands:
        regex, func = command
        if find(message, identifier + regex + "$") and owner in message.split("!")[0].replace(":", ""):
            info = {"ch": name, "msg": message}
            log(message)
            func(info)
            break

    for char in rules.keys():
        if find(message, char):
            log(message)
            send_prv_msg(name, rules[char])


log("===== Carbon Start =====")

raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
raw_socket.connect((server, port))

if isSsl:
    sock = ssl.wrap_socket(raw_socket)
else:
    sock = raw_socket

send("USER " + nick + " " + nick + " " + nick + " :Carbon, IRC bot by Graphene" + nl)
send("NICK " + nick + nl)

print("logging in...")

while True:
    msg = sock.recv(512).decode(codec).strip("\r\n")
    log(msg)
    
    if "MODE" in msg or "MOTD" in msg:
        break
    
    if msg.find("PING :") != -1:
        ping(msg)
    
for ch in channel:
    join(ch)

print("Logged in succesfully!")
    
while True:
    try:
        msg = sock.recv(2048).decode(codec).strip("\r\n")  
        if "PING :" in msg:
            ping(msg)
        else:
            print (msg)
            ch = msg.split("PRIVMSG")[-1].split(" :")[0]
            execute(ch, msg)

    except Exception as e:
        print("Error Occured: " + str(e))
        log(str(e))
        try:
            for ch in channel:
                send_prv_msg(ch, "Sorry, something went wrong!: " + str(e))
        except Exception as e:
            break

sock.close()